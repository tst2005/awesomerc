local private = {}

private.user = { name = "unlogic",
                 city = "Gjovik",
                 country = "Norway",
                 time_offset = 2,
                 loc = { lat = 60.79, lon = 10.69 } }

private.weather = { api_key = "6f562795dda8824f99be11ea1eb0e77e" }

return private
